<?php

namespace XLabs\NotifyBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('x_labs_notify');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        $rootNode
            ->children()
                ->arrayNode('clients')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('schema')->isRequired()->end()
                            ->scalarNode('ssl_key')->isRequired()->end()
                            ->scalarNode('ssl_cert')->isRequired()->end()
                            ->scalarNode('host')->isRequired()->end()
                            ->scalarNode('port')->isRequired()->end()
                            ->scalarNode('enabled')->defaultFalse()->end()
                            //->scalarNode('track_online_users')->defaultFalse()->end()
                            ->scalarNode('version')
                                ->defaultValue('1.x')
                                ->validate()
                                    ->ifNotInArray(array('1.x', '0.x'))
                                        ->thenInvalid('Invalid version number "%s"')
                                    ->end()
                            ->end()
                            ->arrayNode('redis_settings')->addDefaultsIfNotSet()
                                ->children()
                                    ->scalarNode('host')->defaultValue('127.0.0.1')->end()
                                    ->integerNode('port')->defaultValue(6379)->end()
                                    ->integerNode('database_id')->defaultValue(6)->end()
                                ->end()
                            ->end()
                            ->scalarNode('_key_namespace')->defaultValue('xlabs:notifications')->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
