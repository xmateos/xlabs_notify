<?php

namespace XLabs\NotifyBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Process\Process;

/**
 * Class RunServerCommand
 * @package XLabs\NotifyBundle\Command
 * Runs NodeJs server JS file
 */
class RunServerCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('xlabs_notify:server:run')
            /*->addOption(
                'job_id',
                null,
                InputOption::VALUE_REQUIRED,
                'Job ID',
                false
            )*/
            ->setDescription('Runs NodeJs server JS file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        $configFile = __DIR__.'/../Resources/public/notifier.js';

        $returnCode = 0;
        if(!is_file($configFile))
        {
            $command = $this->getApplication()->find('xlabs_notify:server:create');
            $arguments = array(
                'command' => 'xlabs_notify:server:create',
                //'name'    => 'Fabien',
                //'--yell'  => true,
            );
            $cmd_args = new ArrayInput($arguments);
            $returnCode = $command->run($cmd_args, $output);
        }

        if($returnCode == 0)
        {
            $cmd = 'node '.$configFile;
            $process = new Process($cmd);
            $process->setTimeout(NULL);
            $buffer = false;
            $callback = function($buffer){};
            $background_process = false;
            $returnOutput = true;

            if($background_process)
            {
                $process->start();
            } else {
                $process->run(function($type, $buff) use ($buffer, $callback) {
                    if (Process::ERR === $type) {
                        $buffer = $buff;
                    } else {
                        $buffer = $buff;
                    }
                    if($callback && is_callable($callback))
                    {
                        $callback($buffer);
                    }
                });
            }

            if(!$process->isSuccessful())
            {
                if($process->getErrorOutput())
                {
                    $output->writeln("\033[1;32m".$process->getErrorOutput()."\033[0m\n\r");
                }
                if($process->getOutput())
                {
                    $output->writeln("\033[1;32m".$process->getOutput()."\033[0m\n\r");
                }
            }

            return $returnOutput ? $process->getOutput() : $process->isSuccessful();
        }
    }
}