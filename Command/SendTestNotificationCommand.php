<?php

namespace XLabs\NotifyBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Process\Process;

/**
 * Class SendTestNotificationCommand
 * @package XLabs\NotifyBundle\Command
 */
class SendTestNotificationCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('xlabs_notify:send')
            ->addOption(
                'uid',
                null,
                InputOption::VALUE_REQUIRED,
                'User ID',
                false
            )
            ->setDescription('Runs NodeJs server JS file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $param_uid = $input->getOption('uid');
        $container = $this->getApplication()->getKernel()->getContainer();

        $xlabs_notify = $container->get('xlabs_notifier');
        //dump($xlabs_notify->getSocket());
        $xlabs_notify->testNotification(array(
            'destinatary_id' => $param_uid ? $param_uid : '1048',
            'title' => 'Test notification',
            'msg' => 'Test text for message',
            'icon' => '/bundles/xlabsnotify/xlabs.png',
            'mute' => false,
            'link' => false,
            //'sound' => false,
            'fadeout_after_ms' => 1500
        ));
    }
}