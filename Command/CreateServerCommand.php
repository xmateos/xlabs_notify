<?php

namespace XLabs\NotifyBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CreateServerCommand
 * @package XLabs\NotifyBundle\Command
 * Creates NodeJs server JS file
 */
class CreateServerCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('xlabs_notify:server:create')
            /*->addOption(
                'job_id',
                null,
                InputOption::VALUE_REQUIRED,
                'Job ID',
                false
            )*/
            ->setDescription('Creates NodeJs server JS file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /*
         * This command will delete a job from DB
         */
        //$param_job_id = $input->getOption('job_id');

        $container = $this->getApplication()->getKernel()->getContainer();
        $config = $container->getParameter('xlabs_notify_config');
        $config = $config['clients']['notifier'];

        $configStr = <<<EOT
var app = require('express')();
var express = require('express');
// HTTP
/*var instance = require('http').Server(app);
var io = require('socket.io')(instance);*/
// HTTPS
var fs = require('fs');
var options = {
    key: fs.readFileSync('{$config['ssl_key']}'),
    cert: fs.readFileSync('{$config['ssl_cert']}'),
    requestCert: true
};
var instance = require('https').createServer(options, app);
var io = require('socket.io')(instance);

/*
Redis will allow multiple node.js server instances to communicate with each other (scalability)
 */
var redis = require('socket.io-redis');
io.adapter(redis({ host: '{$config['redis_settings']['host']}', port: {$config['redis_settings']['port']} }));

var users = {};
var frontend_users = {}; // friendly array of ID/USERNAME

io.on('connection', function(socket){
    socket.on('disconnect', function(reason) {
        if(socket.custom_id !== undefined)
        {
            if(users[socket.custom_id].db_id in frontend_users) delete frontend_users[users[socket.custom_id].db_id];
            if(socket.custom_id in users) delete users[socket.custom_id];
            io.sockets.emit('xlabs_refresh_users', {
                users: frontend_users
            });
        }
    });

    socket.on('xlabs_login', function(userData) {
        if(users[userData['id']] == undefined)
        {
            socket.custom_id = userData['id'];
            users[userData['id']] = {
                'id': userData['id'],
                'db_id': userData['db_id'],
                'username': userData['username'],
                'avatar': userData['avatar'],
                'socket': socket.id
            };
            /*frontend_users.push({
                'id': userData['db_id'],
                'username': userData['username']
            });*/
            frontend_users[userData['db_id']] = {
                'id': userData['db_id'],
                'username': userData['username']
            };
        } else {
            users[userData['id']]['socket'] = socket.id;
        }
        // send everybody except the sender
        /*socket.broadcast.emit('xlabs_login', {
            'icon': '/bundles/xlabsspider/nodejs/notifications/spider.png',
            'msg': 'User is online'
        });*/
        io.sockets.emit('xlabs_refresh_users', {
            users: frontend_users
        });
    });

    socket.on('xlabs_notification', function(data){
        //data.sender_id = socket.custom_id in users ? users[socket.custom_id].db_id : false;
        if(data.destinatary_id)
        {
            if(data.destinatary_id in users)
            {
                io.to(users[data.destinatary_id]['socket']).emit('xlabs_notification', data);
            }
        } else {
            io.sockets.emit('xlabs_notification', data);
        }
    });

    socket.on('xlabs_play_sound', function(data){
        if(data.destinatary_id)
        {
            if(data.destinatary_id in users)
            {
                io.to(users[data.destinatary_id]['socket']).emit('xlabs_play_sound', data);
            }
        } else {
            io.sockets.emit('xlabs_play_sound', data);
        }
    });

    socket.on('xlabs_speak_text', function(data){
        if(data.destinatary_id)
        {
            if(data.destinatary_id in users)
            {
                io.to(users[data.destinatary_id]['socket']).emit('xlabs_speak_text', data);
            }
        } else {
            io.sockets.emit('xlabs_speak_text', data);
        }
    });

    socket.on('xlabs_custom_message', function(data){
        if(data.destinatary_id)
        {
            if(data.destinatary_id in users)
            {
                io.to(users[data.destinatary_id]['socket']).emit('xlabs_custom_message', data);
            }
        } else {
            io.sockets.emit('xlabs_custom_message', data);
        }
    });
});

instance.listen({$config['port']}, function(){
    // echo is not allowed when instantiated from PHP
    console.log('XLabs Notifications listening on *:{$config['port']}');
});

/*
// sending to sender-client only
socket.emit('message', "this is a test");

// sending to all clients, include sender
io.emit('message', "this is a test");

// sending to all clients except sender
socket.broadcast.emit('message', "this is a test");

// sending to all clients in 'game' room(channel) except sender
socket.broadcast.to('game').emit('message', 'nice game');

// sending to all clients in 'game' room(channel), include sender
io.in('game').emit('message', 'cool game');

// sending to sender client, only if they are in 'game' room(channel)
socket.to('game').emit('message', 'enjoy the game');

// sending to all clients in namespace 'myNamespace', include sender
io.of('myNamespace').emit('message', 'gg');

// sending to individual socketid
socket.broadcast.to(socketid).emit('message', 'for your eyes only');
*/
EOT;

        //$configFile = $container->getParameter('kernel.root_dir') . '/../web/bundles/xlabsnotify/notifier.js';
        $configFile = __DIR__.'/../Resources/public/notifier.js';
        file_put_contents($configFile, $configStr);
        chmod($configFile, 0775);

        //$template = $container->getParameter('kernel.root_dir') . '/../src/bundles/mybundle/myfiles';
    }
}