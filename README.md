A redis driven like engine.

## Installation ##

Install through composer:
<!--
```bash
# Symfony 2.8
php -d memory_limit=-1 composer.phar require xlabs/notifybundle "~1.0"

# Symfony 3.4
php -d memory_limit=-1 composer.phar require xlabs/notifybundle "~2.0"
```
-->
```bash
php -d memory_limit=-1 composer.phar require xlabs/notifybundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new XLabs\NotifyBundle\XLabsNotifyBundle(),
    ];
}
```

## Routing

Append to main routing file:

``` yml
# app/config/routing.yml
  
x_labs_notify:
    resource: "@XLabsNotifyBundle/Resources/config/routing.yml"
    prefix:   /
```

## Configuration sample
Default values are shown below:
``` yml
# app/config/config.yml
  
x_labs_notify:
    clients:
        # many clients allowed; default is named "default"
        notifier:
            schema: https
            ssl_key: /vagrant/sites/stiffia/ssl/wildcard.stiffia.com.key
            ssl_cert: /vagrant/sites/stiffia/ssl/wildcard.stiffia.com.crt
            host: notifications.stiffia.com
            port: 3004
            enabled: true
            #track_online_users: true
            # specify version 0.x for 0.* version and 1.x for 1.0 version
            # to know socket.io version -> npm list socket.io
            version: 1.x
            # Storage
            redis_settings:
                host: 192.168.5.23
                port: 6379
                database_id: 8
            _key_namespace: 'xlabs:notifications'
```
Make sure you update all assets:
```bash
php app/console assets:install --symlink
```

### Usage ###
Append this before the end of body tag in your template:
```php
{{ render(controller('XLabsNotifyBundle:Notifier:loader')) }}
```

For a full sample template, check:
```php
XLabsFollowBundle:Follow:example.html.twig
```

### Render user notifications ###
Append this anywhere in your template:
```php
{% include 'XLabsNotifyBundle:Notifier:notifications.html.twig' with {
    'max_results': <num_results>
} %}
```
Ajax driven pagination will be automatically added.
  
If you want to render notifications your own way, override the template by creating:
```php
app/Resources/XLabsNotifyBundle/views/Notifier/notification.html.twig
```
To override the "no results" template:
```php
app/Resources/XLabsNotifyBundle/views/Notifier/no_results.html.twig
```
To remove (ajax) a notification, add to your DOM element the following attribute:
```php
'data-xlabs-notify-remove="<notification_id>"'
```
To remove (ajax) all notifications, add to your DOM element the following attribute:
```php
'data-xlabs-notify-remove="all"'
```
To mark as read (ajax) a notification, add to your DOM element the following attribute:
```php
'data-xlabs-notify-markAsRead="<notification_id>"'
```
You can also define a custom JS callback function  for all these operations above by adding the following attribute, so it would be executed after the action:
```php
'data-xlabs-callback="YOUR_JS_FUNCTION"'
```

### Notifications cleanup ###
It´s strongly recommended that you create a daily cronjob to remove from user notifications all the items that have automatically expired. To do so:
```php
$xlabs_storage = $container->get('xlabs_notifier_storage');
foreach($users as $user)
{
    $xlabs_storage->clearExpiredUserNotifications($user->getId())
}
```

### Requirements ###
The node js connection is made through a nginx reverse proxy. Make sure to set a nginx vhost:
```yml
server {
    listen 443 ssl;

    server_name <x_labs_chat.nodejs_settings.host>;

    ## SSL settings
    ssl on;
    ssl_certificate <x_labs_chat.nodejs_settings.ssl_cert>;
    ssl_certificate_key <x_labs_chat.nodejs_settings.ssl_key>;

    ## SSL caching/optimization
    ssl_protocols        SSLv3 TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers RC4:HIGH:!aNULL:!MD5;
    ssl_prefer_server_ciphers on;
    keepalive_timeout    60;
    ssl_session_cache    shared:SSL:10m;
    ssl_session_timeout  10m;

	location / {
        #proxy_set_header 'Access-Control-Allow-Origin' '*';

        #proxy_set_header X-Real-IP $remote_addr;
        #proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        #proxy_set_header Host $http_host;
        #proxy_set_header X-NginX-Proxy true;
        #proxy_redirect off;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;

        proxy_pass https://<your_internal_nodejs_server_ip>:<x_labs_chat.nodejs_settings.port>;
    }
}
```