<?php

namespace XLabs\NotifyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class ExampleController extends Controller
{
    public function indexAction()
    {
        $n = $this->get('xlabs_notifier');
        /*$n->showNotification(array(
            'destinatary_id' => 1,
            'title' => 'PHP title',
            'msg' => 'PHP message',
            'icon' => '/bundles/xlabsnotify/xlabs.png',
            'mute' => false,
            'link' => false,
            'sound' => false
        ));*/
        /*$n->playSound(array(
            'destinatary_id' => 1,
            'file' => '/bundles/xlabsnotify/sound/arpeggio.mp3',
        ));*/
        /*$n->speakText(array(
            'destinatary_id' => 1,
            'text' => 'Go fuck you',
        ));*/
        /*$n->customMessage(array(
            'destinatary_id' => 1,
            'callback' => false,
            'sample_param' => 'puta'
        ));*/
        //die;
        $s = $this->get('xlabs_notifier_storage');
        /*$s->push(array(
            'destinatary_id' => 1,
            'msg' => 'This is a test stored notification.'
        ));
        $s->push(array(
            'destinatary_id' => 1,
            'msg' => 'This is a test stored notification 2.'
        ));
        $s->push(array(
            'destinatary_id' => 1,
            'msg' => 'This is a test stored notification 4.'
        ));*/
        //dump($s->getUserNotifications(1)); die;

        return $this->render('XLabsNotifyBundle:Example:index.html.twig');
    }
}
