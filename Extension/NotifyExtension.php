<?php

namespace XLabs\NotifyBundle\Extension;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use XLabs\NotifyBundle\Services\Storage;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class NotifyExtension extends AbstractExtension
{
    private $token_storage;
    private $xlabs_notifier_storage;

    public function __construct(TokenStorageInterface $token_storage, Storage $xlabs_notifier_storage)
    {
        $this->token_storage = $token_storage;
        $this->xlabs_notifier_storage  = $xlabs_notifier_storage;
    }
    
    public function getFunctions()
    {
        return array(
            new TwigFunction('getTotalUserNotifications', array($this, 'getTotalUserNotifications')),
        );
    }
    
    public function getFilters()
    {
        return array();
    }

    public function getTotalUserNotifications($user_id, $not_read_only = false)
    {
        $user = $this->token_storage->getToken()->getUser();
        return is_string($user) ? 0 : $this->xlabs_notifier_storage->getTotalUserNotifications($user_id, $not_read_only);
    }
}