<?php

namespace XLabs\NotifyBundle\Services;

use XLabs\NotifyBundle\Client\Client as Elephant;

class Notifier
{
    /*
     * Service to generate NodeJS notifications
     */
    private $config;
    private $socket;

    public function __construct($config, Elephant $socket)
    {
        $this->config = $config['clients']['notifier'];
        $this->socket = $socket;
    }

    public function closeSocket()
    {
        $this->socket->getElephantIO()->close();
    }

    public function isAlive()
    {
        $notifier = $this->config;
        // PHP < 5.6
        //$fp = @fsockopen(($notifier['schema'] == 'https' ? 'ssl://' : '').$notifier['host'], $notifier['port'], $errno, $errstr);
        // PHP > 5.6
        $stream_context_options = [
            'ssl' => [
                'verify_peer' => true,
                'cafile' => '/usr/lib/ssl/cert.pem',
                //'CN_match' => $notifier['host'], # deprecated in favor of 'peer_name'
                'peer_name' => $notifier['host'],
            ]
        ];
        $stream_context = stream_context_create($stream_context_options);
        $fp = @stream_socket_client('ssl://'.$notifier['host'].':'.$notifier['port'], $errno, $errstr, 10, STREAM_CLIENT_CONNECT, $stream_context);
        return $fp ? true : false;
    }

    public function showNotification($aOptions)
    {
        if(!$this->config['enabled'] || !$this->isAlive())
        {
            return false;
        }

        $default_options = array(
            'destinatary_id' => false,
            'title' => false,
            'msg' => 'Test text for message',
            'icon' => '/bundles/xlabsnotify/xlabs.png',
            'mute' => false,
            'link' => false,
            'sound' => false,
            'fadeout_after_ms' => false
        );
        $aOptions = array_merge($default_options, $aOptions);
        $aOptions['destinatary_id'] = $aOptions['destinatary_id'] ? md5($aOptions['destinatary_id']) : $aOptions['destinatary_id'];

        $this->socket->send('xlabs_notification', $aOptions);
        $this->closeSocket();
    }

    public function playSound($aOptions)
    {
        if(!$this->config['enabled'] || !$this->isAlive())
        {
            return false;
        }

        $default_options = array(
            'destinatary_id' => false,
            'file' => false,
        );
        $aOptions = array_merge($default_options, $aOptions);
        $aOptions['destinatary_id'] = $aOptions['destinatary_id'] ? md5($aOptions['destinatary_id']) : $aOptions['destinatary_id'];

        $this->socket->send('xlabs_play_sound', $aOptions);
        $this->closeSocket();
    }

    public function speakText($aOptions)
    {
        if(!$this->config['enabled'] || !$this->isAlive())
        {
            return false;
        }

        $default_options = array(
            'destinatary_id' => false,
            'text' => false,
        );
        $aOptions = array_merge($default_options, $aOptions);
        $aOptions['destinatary_id'] = $aOptions['destinatary_id'] ? md5($aOptions['destinatary_id']) : $aOptions['destinatary_id'];

        $this->socket->send('xlabs_speak_text', $aOptions);
        $this->closeSocket();
    }

    public function customMessage($aOptions)
    {
        if(!$this->config['enabled'] || !$this->isAlive())
        {
            return false;
        }

        $default_options = array(
            'destinatary_id' => false,
            'callback' => ''
        );
        $aOptions = array_merge($default_options, $aOptions);
        $aOptions['destinatary_id'] = $aOptions['destinatary_id'] ? md5($aOptions['destinatary_id']) : $aOptions['destinatary_id'];

        $this->socket->send('xlabs_custom_message', $aOptions);
        $this->closeSocket();
    }

    public function testNotification($aOptions)
    {
        // same as showNotification but skipped checking socket availability
        $default_options = array(
            'destinatary_id' => false,
            'title' => false,
            'msg' => 'Test text for message',
            'icon' => '/bundles/xlabsnotify/xlabs.png',
            'mute' => false,
            'link' => false,
            'sound' => false,
            'fadeout_after_ms' => false
        );
        $aOptions = array_merge($default_options, $aOptions);
        $aOptions['destinatary_id'] = $aOptions['destinatary_id'] ? md5($aOptions['destinatary_id']) : $aOptions['destinatary_id'];

        $this->socket->send('xlabs_notification', $aOptions);
        $this->closeSocket();
    }

    /*public function getNotificationIcon()
    {
        return '/bundles/xlabsspider/nodejs/notifications/spider.png';
    }

    public function getErrorNotificationIcon()
    {
        return '/bundles/xlabsspider/nodejs/notifications/error.png';
    }*/
}