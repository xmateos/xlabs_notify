<?php

namespace XLabs\NotifyBundle\Services;

use Predis\Client as Predis;

class Storage
{
    private $config;
    private $redis;
    private $default_ttl;

    public function __construct($config)
    {
        $this->config = $config['clients']['notifier'];
        $this->redis = new Predis(array(
            'scheme' => 'tcp',
            'host'   => $this->config['redis_settings']['host'],
            'port'   => $this->config['redis_settings']['port'],
            'database' => $this->config['redis_settings']['database_id']
        ), array(
            'prefix' => isset($this->config['_key_namespace']) ? $this->config['_key_namespace'].':' : ''
        ));
        $this->default_ttl = strtotime("+3 days");
    }

    /**
     * @param $aData
     * Store notification
     */
    public function push($aData)
    {
        $now = time();
        //$notification_id = $this->redis->incr('id');

        // Exclude the TTL parameter cause it can generate multiple notifications when user, for instance, likes/unlikes/likes/unlikes again...
        $uniqueKey = $aData;
        if(isset($uniqueKey['ttl']))
        {
            unset($uniqueKey['ttl']);
        }
        $notification_id = md5(json_encode($uniqueKey));

        $aData['id'] = $notification_id;
        $aData['createdAt'] = $now;
        $aData['read'] = false;
        $aData['msg'] = isset($aData['msg']) ? $aData['msg'] : '';
        $this->redis->hmset($notification_id, $aData);

        // Add to user´s notifications
        $score = $now;
        $this->redis->zAdd("user:".$aData['destinatary_id'], array($notification_id => $score));

        // Expiration
        $ttl = isset($aData['ttl']) ? $aData['ttl'] : $this->default_ttl;
        $this->redis->expireat($notification_id, $ttl);
    }

    /**
     * @param $user_id
     * @param $maxResults
     * @param $page
     * @return array|bool
     * Get all notifications for a user
     */
    public function getUserNotifications($user_id, $maxResults = false, $page = 1)
    {
        if(!$user_id) return array();
        $offset = $maxResults ? (($page - 1)*$maxResults) : 0;
        $lastItem = $maxResults ? ($offset + ($maxResults - 1)) : -1;

        $notification_ids = array_filter($this->redis->zRevRange("user:".$user_id, $offset, $lastItem), function($v){
            return $v != 'undefined' && $this->redis->exists($v);
        });
        $notifications = array_map(function($v) use ($user_id){
            if($this->redis->exists($v))
            {
                $data = $this->redis->hgetall($v);
                return array_key_exists('id', $data) ? $data : false;
                //return isset($data['id']) ? $data : false;
            }
            return false;
        }, $notification_ids);
        return array_values(array_filter($notifications));
    }

    /**
     * @param $user_id
     * @return int
     */
    public function clearExpiredUserNotifications($user_id)
    {
        if(!$user_id) return 0;
        $notification_ids = array_filter($this->redis->zRevRange("user:".$user_id, 0, -1), function($v){
            $elem = $this->redis->hgetall($v);
            return $v == 'undefined' || !$this->redis->exists($v) || !array_key_exists('id', $elem);
        });
        $count = 0;
        foreach($notification_ids as $notification_id)
        {
            $count++;
            $this->removeUserNotification($user_id, $notification_id);
        }
        return $count;
    }

    public function getTotalUserNotifications($user_id, $not_read_only = false)
    {
        if(!$user_id) return 0;
        $notification_ids = array_filter($this->redis->zRevRange("user:".$user_id, 0, -1), function($v){
            return $v != 'undefined' && $this->redis->exists($v);
        });
        $notifications = array_map(function($v) use ($not_read_only){
            if($not_read_only)
            {
                if($this->redis->exists($v))
                {
                    $data = $this->redis->hgetall($v);
                    return (array_key_exists('id', $data) && !$data['read']) ? $v : false;
                } else {
                    return false;
                }
            } else {
                if($this->redis->exists($v))
                {
                    $data = $this->redis->hgetall($v);
                    return array_key_exists('id', $data) ? $v : false;
                } else {
                    return false;
                }
            }
        }, $notification_ids);
        return count(array_filter($notifications));
    }

    /**
     * @param $notification_id
     * @return array
     */
    public function pull($notification_id)
    {
        return $this->redis->hgetall($notification_id);
    }

    /**
     * @param $notification_id
     * @return int
     */
    public function remove($notification_id)
    {
        return $this->redis->del(array($notification_id));
    }

    public function removeUserNotification($user_id, $notification_id)
    {
        $this->redis->zRem("user:".$user_id, $notification_id);
        return $this->remove($notification_id);
    }

    /**
     * @param $notification_id
     * @return int
     */
    public function markAsRead($notification_id)
    {
        return $this->redis->exists($notification_id) ? $this->redis->hset($notification_id, 'read', true) : false;
    }
}